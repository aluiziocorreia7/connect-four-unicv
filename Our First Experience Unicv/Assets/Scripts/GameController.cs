using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{   
    int[,] Board;
    [SerializeField]
    GameObject[] piecePrefab;
    GameObject lastPiece;
    bool gameEnded;
    // Start is called before the first frame update
    void Start()
    {
        Board = new int[7,6];
        InitializeBoard();
        lastPiece = GameObject.Find("redPlayer");
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnEnable(){
        PieceController.OnMove += MakeMove;
    }

    void OnDisable()
    {
        PieceController.OnMove -= MakeMove;
    }

    void MakeMove(int player, int col){
        
        bool movedone = false;
        for(int l = 0; l < 6; l++) {
            if(Board[col, l] == 0){
                Board[col, l] = player;
                movedone = true;
                break;
            }
        }
        if(movedone){
            lastPiece.GetComponent<PieceController>().enabled =  false;
             VerifyaWinner(player);
            if(!gameEnded){
                
                int index = player == 1 ? 1 : 0; //player 2 played, instantiate player 1 (index 0) and vice versa
                InstantiatePiece(index);
            }else{
                Debug.Log("Player: "+player+" Won");
            }
        }
    }

    void VerifyaWinner(int pl){
        for(int l = 0; l < 6; l++){
            if(WinnerIntheLine(pl,l)){
                gameEnded = true;
                break;
                //OnWinner(pl);
            }
        }
        for(int c = 0; c < 7; c++){
            if(WinnerIntheColumn(pl,c)){
                gameEnded = true;
                break;
                //OnWinner(pl);
            }
        }
        /*for(int l = 0; l < 6; l++){
            if(WinnerIntheLine(pl,l)){
                gameEnded = true;
                //OnWinner(pl);
            }
        }*/

        
    }


    bool WinnerIntheColumn(int pl, int column){
        int concurrents = 0; //number of consecutive columns in this line with same player's piece
        for(int l = 0; l < 6; l++){
            if( Board[column,l] != pl){
            
               concurrents = 0; //go back to zero
            }else{
                concurrents++;
               if(concurrents == 4)return true;
            }
        }
        return false;
    }


    bool WinnerIntheLine(int pl, int line){
        int concurrents = 0; //number of consecutive columns in this line with same player's piece
        for(int c = 0; c < 7; c++){
            if( Board[c,line] != pl){
            
               concurrents = 0; //go back to zero
            }else{
                concurrents++;
               if(concurrents == 4)return true;
            }
        }
        
        return false;
    }


    void InitializeBoard(){
        gameEnded = false;
        for(int c = 0; c < 7; c++) {
            for(int l = 0; l < 6; l++) {
                Board[c, l] = 0; 
                     
            } 
        }
    }

    void InstantiatePiece(int index){
        lastPiece = Instantiate(piecePrefab[index]);
    }
}
