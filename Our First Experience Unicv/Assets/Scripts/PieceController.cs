using UnityEngine;

public class PieceController : MonoBehaviour
{   
    [SerializeField]
    int player;
    public delegate void Move(int pl, int col);
    public static Move OnMove;
    #region Drag&Drop
    private bool isDragging = false;
    private Vector3 offset;
    private bool validMove;
    private Vector3 initialPos;
    private float speed;
    private Rigidbody2D rb;

    void Start(){
        speed = 10;
        initialPos = transform.position;
        rb = GetComponent<Rigidbody2D>();
        validMove = false;
    }

    void OnMouseDown()
    {
        
        offset = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
       
        isDragging = true;
    }

    void OnMouseUp()
    {
        isDragging = false;
        if(validMove){
            rb.isKinematic = false;
        }else{
            Vector3.MoveTowards(transform.position, initialPos, speed*Time.deltaTime);
        }
    }

    void Update()
    {   
        if (isDragging)
        {   
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, offset.z);
            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
            
            transform.position = curPosition;
        }
    }
    #endregion


    void OnTriggerEnter2D(Collider2D other)
    {   
       
        if(other.CompareTag("column")){
            
            if(!validMove)return;
            int colindex = 0;
            int.TryParse(other.gameObject.name, out colindex);
            colindex -= 1;
            if(colindex >= 0){
                OnMove(player, colindex);
                //fazer jogada
            }
        }else if(other.gameObject.name.Equals("AreaValido")){
            validMove = true;
        }
        
    }


    void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.name.Equals("AreaValido")){
            if(rb.isKinematic){
               
                validMove = false;
            }
        }
    }

    

   
}
