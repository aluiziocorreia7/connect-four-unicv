using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{   
    [SerializeField]
    float Speed;
    Dictionary<KeyCode, Vector3> Directions;
    // Start is called before the first frame update
    void Start()
    {
        Directions = new Dictionary<KeyCode, Vector3>(){
            {KeyCode.J, Vector3.left},
            {KeyCode.I, Vector3.up},
            {KeyCode.K, Vector3.down},
            {KeyCode.L, Vector3.right}
        };

    }

    // Update is called once per frame
    void Update()
    {
        foreach(var direction in Directions.Keys) {
            
            if(Input.GetKey(direction)){
                
                this.transform.Translate(Directions[direction] * Speed *Time.deltaTime, Space.Self);
            }
        }
    }
}
